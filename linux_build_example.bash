#!/bin/bash

# set the JAVA_HOME environment variable
OS_NAME=$(head -n 1 /etc/os-release)
echo "Detected OS $OS_NAME"
JAVA_HOME=
if [ "${OS_NAME}" == "NAME=\"CentOS Linux\"" ]
then
	JAVA_HOME=/usr/lib/jvm/java-1.7.0/
else	
	JAVA_HOME=/usr/lib/jvm/java-1.7.0-openjdk-amd64
fi
export JAVA_HOME

# Clean previous build
ant clean
cp build/c/libnautygetcanLinux64.so cp build/c/old_libnautygetcanLinux64.so

# Build nauty
ant build_nauty
cp build/c/libnautygetcanLinux64.so lib/

# Compile java and build jars
ant build
ant dist

# Perform some tests
cd test
python fragmentTest.py
python noFragmentTest.py
python nitrogenValenceTest.py
python blockedAtomTest.py
cd ..
