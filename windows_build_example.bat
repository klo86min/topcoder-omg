:: Setting up environment variables
set ANT_HOME=C:\ant
set JAVA_HOME=C:\Program Files\Java\jdk1.7.0_79
set PYTHON_HOME=C:\Python27
set PATH=%PATH%;%ANT_HOME%\bin;%JAVA_HOME%\bin;%PYTHON_HOME%
:: Delete previous build
call ant clean
copy build\c\libnautygetcanWin32.dll build\c\old_libnautygetcanWin32.dll
copy build\c\libnautygetcanWin64.dll build\c\old_libnautygetcanWin64.dll
:: Rebuild dll for 32 and 64 bits
call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" x86
call ant build_nauty_32
copy build\c\libnautygetcanWin32.dll lib\
call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" amd64
call ant build_nauty_64
copy build\c\libnautygetcanWin64.dll lib\
:: Compile Java code
call ant build
:: Build OMG.jar and OMG_GUI.jar
call ant dist
:: Perform some tests
cd test
call python noFragmentTest.py
call python fragmentTest.py
call python nitrogenValenceTest.py
call python blockedAtomTest.py
cd ..
