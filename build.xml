<?xml version="1.0"?>

<project name="Open Molecule Generator" default="dist" basedir=".">
  <!-- Import the dependencies of this build file -->
  <import file="${basedir}/build-dependencies.xml"/>
  
  <!-- COMPONENT PARAMETERS -->
  <property name="component" value="OMG"/>
  <property name="package" value="org.omg"/>
  <property name="packagedir" value="org/omg"/>
  <property name="distfilename" value="OMG"/>

  <!-- DIRECTORY SETUP -->
  <property name="srcdir" value="src"/>
  <property name="javasrc" value="${srcdir}/java"/>
  <property name="csrc" value="${srcdir}/c"/>

  <property name="builddir" value="build" />
  <property name="build_java" value="${builddir}/java" />
  <property name="build_c" value="${builddir}/c" />
  <property name="build_distdir" value="${builddir}/dist" />

  <!-- COMPONENT DISTRIBUTION STRUCTURE -->
  <property name="component-omg.jar" value="${build_distdir}/${distfilename}.jar" />
  <property name="component-omg-gui.jar" value="${build_distdir}/${distfilename}_GUI.jar" />

  <!-- EXECUTION TAGS -->
  <property name="debug" value="true"/>
  <property name="verbose" value="no"/>

  <property environment="env"/>

  <!-- Clean output folder -->
  <target name="clean">
    <delete failonerror="false" dir="${builddir}" includeemptydirs="true"/>
  </target>
  
  <!-- Compile the java code -->
  <target name="compile_java">
    <mkdir dir="${build_java}"/>
    <javac
           srcdir="${javasrc}"
           destdir="${build_java}"
           includes="**/*.java"
           debug="${debug}"
           verbose="${verbose}"
           includeantruntime="false"
           source="1.7"
           target="1.7">
      <classpath refid="buildlibs"/>
    </javac>
  </target>

  <!-- Prepare nauty build -->
  <target name="build_nauty_common" depends="compile_java">
    <mkdir dir="${build_c}"/>
    <javah destdir="${build_c}" class="org.omg.OMGJNI" classpath="${build_java}" />
    <copy todir="${build_c}">
      <fileset dir="${csrc}"/>
    </copy>
  </target>

  <!-- Build the nauty library for 64 bit platform -->
  <target name="build_nauty_64" depends="build_nauty_common">
    <antcall target="compile_for_mac_apple_JDK" />
    <antcall target="compile_for_mac_oracle_JDK" />
    <antcall target="compile_for_unix" />
    <antcall target="compile_for_windows_64" />
  </target>

  <!-- Build the nauty library for 32 bit platform (Windows only) -->
  <target name="build_nauty_32" depends="build_nauty_common">
    <antcall target="compile_for_windows_32" />
  </target>

  <!-- Compile C code for mac only if we are running on mac with Apple JDK -->
  <target name="compile_for_mac_apple_JDK" if="is_mac_apple_JDK">
    <exec dir="${build_c}" executable="cc">
      <arg line="-c -I/System/Library/Frameworks/JavaVM.framework/Headers nautygetcan.c nauty.c nautil.c naututil.c naugraph.c rng.c nausparse.c schreier.c"/>
    </exec>
    <exec dir="${build_c}" executable="cc">
      <arg line="-dynamiclib -o libnautygetcanMac64.jnilib nautygetcan.o nauty.o nautil.o naututil.o naugraph.o rng.o nausparse.o schreier.o"/>
    </exec>
  </target>

  <!-- Compile C code for mac only if we are running on mac with Oracle JDK -->
  <target name="compile_for_mac_oracle_JDK" if="is_mac_oracle_JDK">
    <exec dir="${build_c}" executable="cc">
      <arg line="-c -I${env.JAVA_HOME}/include -I${env.JAVA_HOME}/include/darwin nautygetcan.c nauty.c nautil.c naututil.c naugraph.c rng.c nausparse.c schreier.c"/>
    </exec>
    <exec dir="${build_c}" executable="cc">
      <arg line="-dynamiclib -o libnautygetcanMac64.jnilib nautygetcan.o nauty.o nautil.o naututil.o naugraph.o rng.o nausparse.o schreier.o"/>
    </exec>
  </target>

  <!-- Compile C code for unix64 -->
  <target name="compile_for_unix" if="is_unix">
    <exec dir="${build_c}" executable="gcc" failonerror="true">
      <arg line="-O4 -march=corei7-avx -fPIC -o libnautygetcanLinux64.so -shared -I${env.JAVA_HOME}/include -I${env.JAVA_HOME}/include/linux nautygetcan.c nauty.c nautil.c naututil.c naugraph.c rng.c nausparse.c schreier.c"/>
    </exec>
  </target>
  
  <!-- Compile C code for Win32 -->
  <target name="compile_for_windows_32" if="is_windows">
    <exec dir="${build_c}" executable="cl.exe" failonerror="true">
      <arg line="/O2 /D_USRDLL /D_WINDLL /I '${env.JAVA_HOME}/include' /I '${env.JAVA_HOME}/include/win32' nautygetcan.c nauty.c nautil.c naututil.c naugraph.c rng.c nausparse.c schreier.c /link /DLL /OUT:libnautygetcanWin32.dll"/>
    </exec>
  </target>

  <!-- Compile C code for Win64 -->
  <target name="compile_for_windows_64" if="is_windows">
    <exec dir="${build_c}" executable="cl.exe" failonerror="true">
      <arg line="/O2 /D_USRDLL /D_WINDLL /I '${env.JAVA_HOME}/include' /I '${env.JAVA_HOME}/include/win32' nautygetcan.c nauty.c nautil.c naututil.c naugraph.c rng.c nausparse.c schreier.c /link /DLL /OUT:libnautygetcanWin64.dll"/>
    </exec>
  </target>

  <!-- Build the application -->
  <target name="build" depends="compile_java" />

  <!-- Create the application jar file -->
  <target name="dist_omg" depends="build">
    <!-- Discover all dependency jars automatically -->
    <path id="dependencies.path">
      <fileset dir="${libdir}">
        <include name="**/*.jar" />
      </fileset>
    </path>

    <!-- Put up the string with all jars spearated by a space for the manifest -->
    <pathconvert property="manifest.classpath" pathsep=" ">
      <path refid="dependencies.path" />
      <mapper>
        <chainedmapper>
          <flattenmapper />
          <globmapper from="*.jar" to="*.jar" />
        </chainedmapper>
      </mapper>
    </pathconvert>

    <mkdir dir="${build_distdir}"/>

    <!-- Package the final jar -->
    <jar jarfile="${component-omg.jar}">
      <fileset dir="${build_java}"/>

      <!-- Include the nauty binaries for mac and unix platforms -->
      <zipfileset dir="${libdir}"
                  includes="libnautygetcanMac64.jnilib, libnautygetcanLinux64.so, libnautygetcanWin64.dll, libnautygetcanWin32.dll" prefix="org/omg/"/>
      <!-- Include the eclipse custom jar loader -->
      <zipfileset src="${jar-in-jar-loader.zip}" />

      <!-- Include all dependency jars -->
      <zipfileset dir="${libdir}" includes="**/*.jar" />

      <!-- Create the manifest file with necessary attributes -->
      <manifest>
        <attribute name="Main-Class" value="org.eclipse.jdt.internal.jarinjarloader.JarRsrcLoader" />
        <attribute name="Rsrc-Main-Class" value="org.omg.OMG" />
        <attribute name="Class-Path" value="." />
        <attribute name="Rsrc-Class-Path" value="./ ${manifest.classpath}" />
      </manifest>
    </jar>
  </target>

  <!-- Create the application jar files -->
  <target name="dist_omg_gui" depends="build">
    <!-- Discover all dependency jars automatically -->
    <path id="dependencies.path">
      <fileset dir="${libdir}">
        <include name="**/*.jar" />
      </fileset>
    </path>

    <!-- Put up the string with all jars spearated by a space for the manifest -->
    <pathconvert property="manifest.classpath" pathsep=" ">
      <path refid="dependencies.path" />
      <mapper>
        <chainedmapper>
          <flattenmapper />
          <globmapper from="*.jar" to="*.jar" />
        </chainedmapper>
      </mapper>
    </pathconvert>

    <mkdir dir="${build_distdir}"/>

    <!-- Package the final jar -->
    <jar jarfile="${component-omg-gui.jar}">
      <fileset dir="${build_java}"/>

      <!-- Include the nauty binaries for mac and unix platforms -->
      <zipfileset dir="${libdir}"
                  includes="libnautygetcanMac64.jnilib, libnautygetcanLinux64.so, libnautygetcanWin64.dll, libnautygetcanWin32.dll" prefix="org/omg/"/>

      <!-- Include the eclipse custom jar loader -->
      <zipfileset src="${jar-in-jar-loader.zip}" />

      <!-- Include all dependency jars -->
      <zipfileset dir="${libdir}" includes="**/*.jar" />

      <!-- Create the manifest file with necessary attributes -->
      <manifest>
        <attribute name="Main-Class" value="org.eclipse.jdt.internal.jarinjarloader.JarRsrcLoader" />
        <attribute name="Rsrc-Main-Class" value="org.omg.MainPage" />
        <attribute name="Class-Path" value="." />
        <attribute name="Rsrc-Class-Path" value="./ ${manifest.classpath}" />
      </manifest>
    </jar>
  </target>

  <!-- Create the application jar file -->
  <target name="dist" depends="dist_omg, dist_omg_gui"/>

</project>
