import subprocess
import os.path

if __name__ == '__main__':
    # go through files in dir structure
    # for each base dir
    formulas = ['C5H6O2ClF', 'C5H4NCl',  'C6H6O', 'C7H11OCl', 'C7H4O2FCl', 'C8H10O2N4', 'C9H9F3', 'C10H11OF', 'C10H10OClF', 'C11H11ON', 'C12H9OCl', 'C14H14O', 'C15H12O2N1F3']
    # remove the 3 formulas with long computation times
    formulas = ['C5H6O2ClF', 'C5H4NCl',  'C6H6O', 'C7H11OCl', 'C7H4O2FCl', 'C9H9F3', 'C10H11OF', 'C10H10OClF', 'C12H9OCl', 'C14H14O']
    # remove formulas with errors
    formulas = ['C5H6O2ClF', 'C6H6O', 'C7H4O2FCl', 'C9H9F3', 'C10H11OF', 'C10H10OClF', 'C12H9OCl', 'C14H14O' ]
    
    for formula in formulas:
        print 'attempting formula', formula
        base_dir = '../data/' + formula
        good_sdf = base_dir + os.path.sep + 'good.sdf'
        bad_sdf = base_dir + os.path.sep + 'bad.sdf'
        out_sdf = base_dir + os.path.sep + 'out.sdf'
        # run the codes
        args = ['java', '-jar', '../build/dist/OMG.jar', '-ec', formula, '-fr', good_sdf, '-badlist', bad_sdf, '-o', out_sdf ]
        subprocess.call(args)
