OMG - Open Molecule Generator
 
Copyright 2011 The Netherland Metabolomics Center Team License: GPL v3, see License-gpl-3.txt

Introduction
========

You are currently reading the README file for the OMG Project. This project is hosted under http://sourceforge.net/p/openmg

OMG is an open-source tool for the generation of chemical structures, implemented in the programming language Java(tm).
The library is published under terms of the  standard The GNU General Public License (GPL) v3.
This has implications on what you can do with sources and binaries of the OMG library. 
For details, please refer to the file License-gpl-3.txt, which is provided with this distribution.

PLEASE NOTE: OMG is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

System Requirements
========

OMG.jar and OMG_GUI.jar can be built and run in the following OS;

- Ubuntu 64 bits
- Mac OS X 64 bits
- Windows 64 bits (or 32 bits)

Java 7 is required.

Using OMG tool
==============

In order to use the OMG tool in your program, you need to run the jar file via command line and provide some arguments.

Required Parameter

> -ec:  elemental composition of the molecules to be generated.

Optional Parameters

> -o:   SDF file where to store the molecules.

> -fr:  SDF file containing prescribed one or multiple substructures. In the case of multiple substructures, they have to be non-overlapping. 

> -badlist:  SDF file containing prescribed one or multiple bad substructures to filter. In the case of multiple substructures, they have to be non-overlapping. 

> -n: the maximum number of bond for Nitrogen atom, an integer within \[3,5\], the default is 3.

> -2d: generate 2D coordinates for the output molecules

> -bf: use blocked fragment syntax using mol file formal charges


Here are some examples of how to run OMG using the command line: 

1. Generating molecules
    1. Generate molecules for the elemental composition C6H6  
       `java -jar OMG.jar -ec C6H6`

    2. Generate molecules for the elemental composition C6H6 and store them in out_C6H6.sdf  
       `java -jar OMG.jar -ec C6H6 -o out_C6H6.sdf`

2. Generating molecules with prescribed substructure(s)
    1. Generate molecules for the elemental composition C2H5NO2 (glycine) using the prescribed substructure in fragment_CO2.sdf  
        `java -jar OMG.jar -ec C2H5NO2 -fr data/fragment_CO2.sdf -o out_C2H5NO2.sdf`

3. Generating molecules by filtering bad substructure(s)
    1. Generate molecules for the elemental composition C2H5NO2 (glycine) by filtering bad substructures in fragment_CO2.sdf  
        `java -jar OMG.jar -ec C2H5NO2 -badlist data/fragment_CO2.sdf -o out_C2H5NO2.sdf`

4. Generating molecules with 2D coordinates
	1. Generate molecules for the elemental composition C6H6 and store them in out2D_C6H6.sdf with 2D coordinates  
        `java -jar OMG.jar -ec C6H6 -o out2D_C6H6.sdf -2d`
       
5. Generating molecules with prescribed substructure(s) using the blocked fragments syntax.
    1. Generate molecules for the elemental composition C2H5NO2 (glycine) using the blocked fragment fragment_CO2_blocked.sdf  
        `java -jar OMG.jar -ec C2H5NO2 -fr data/fragment_CO2_blocked.sdf -o blocked_C2H5NO2.sdf -bf`


Using OMG_GUI tool
==================

The OMG_GUI program includes the OMG program along with a graphical user interface.

1. Start the program
    1. The 'Elemental composition' field is mandatory, others are optional.
       `java -jar OMG_GUI.jar`

2. Output files

    1. Messages from the 'OMG' program appear in the 'Execution results...' text area.

    2. Pressing 'Stop' will abort the current operation resulting in partial output.

All buttons, entry fields, and checkboxes are directly related to the command line options, see the previous section for more details.

Blocked atoms and fragments
===========================

The fragment file take into account specific hydrogen atoms: Just design some fragments with some H elements. This may then decrease the number of possible molecules and increase speed.

Alternatively, there is the '-bf' option. If set, it uses the charge column of the Atom Block of the V2000 format (6th column, or 2nd column after the atom symbol)
to determine the number of electron available to build new bonds. That is:

- '0' to '4' stands for a formal charge>=0, the atom is blocked and would not allow new bond (it will be saturated with implicit hydrogens).
- '5' stands for -1 formal charge, 1 electron is free for a new bond.
- '6' stands for -2 formal charge, 2 electrons are free for new bond(s).
- '7' stands for -3 formal charge, 3 electrons are free for new bond(s).

This use of the formal charge is just a "hack" to specify which atoms within the fragments are not blocked, then within the generator all formal charges are set to zero.
The python script 'test/blockedFragmentTest.py' provides 2 examples. See results differences in their respective data folders.
Do not used hydrogen elements and "blocked fragments" at the same time in the fragment sdf file, or do it at your own risk as behavior is undefined.

The specification of the V2000 sdf file are available at:
[http://download.accelrys.com/freeware/ctfile-formats/ctfile-formats.zip](http://download.accelrys.com/freeware/ctfile-formats/ctfile-formats.zip)

As blocked fragment definitions use formal charges, it should be possible to design blocked fragments directly from other chemistry tools (using SMILES...).

File structure
==============

1. lib/ : Third-party dependency libraries.
2. src/ : Contains source code of the application and the nauty C library.
3. test/ : Contains sample sdf files that can be used for testing the application.
4. build.xml : Ant build script.
5. build-dependencies.xml : Dependency import file for the ant build script.
6. License-gpl-3.txt : The license file.
7. README.md : This file.

Setup and build
===============

1. Make sure you have Java 7 and Ant installed in your system.
2. Execute `ant build` to compile the app. If you just need the final jar file, you can skip this step and go directly to step 3.
3. Execute `ant dist` to package the app. The output jars will be at build/dist/OMG.jar and build/dist/OMG_GUI.jar
4. Executing `ant clean` will delete the build directory and all build artifacts.

Building nauty
===============

The OMG distribution comes with pre-built nauty binaries for Windows, Mac and Ubuntu64 platforms. If needed, you can rebuild them.

1. Make sure you have Java 7, Ant, Visual Studio 2013 (on Windows), cc (on Mac) and gcc (on Unix) compilers installed in your system.
2. Set JAVA\_HOME env vairable to where your JDK is installed, e.g. "/usr/lib/jvm/java-7-openjdk-amd64" or "C:\Program Files\Java\jdk1.7.0_79".
3. If building for Windows, you need to set various env variables for the Visual Studio compiler and linker.
   The best and easiest way to do it is to run `vcvarsall.bat amd64` (to build for 64 bit) or `vcvarsall.bat x86` (to build for 32 bit).
   You can find the bat file in your Visual Studio distribution, e.g. "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC".
4. Execute `ant build_nauty_64` to build the 64 bit nauty binary for your OS. To build the 32 bit version on Windows, run `ant build_nauty_32` (but remember to also run `vcvarsall.bat x86` first).
   You can find the built nauty binary at build/c
5. Repeat the steps above for all OSes and/or platforms that you need.
6. If you need to repackage the OMG with the new nauty binaries, copy them to the lib folder and follow the steps in the "Setup and build" section.

the 2 files 'linux_build_example.bash' and 'windows_build_example.bat', sum up the commands that can be used to build and test.

Acknowledgment
==============

The application was written by Julio E. Peironcely  
jpeironcely@gmail.com  
Leiden, Netherlands  
http://juliopeironcely.com
